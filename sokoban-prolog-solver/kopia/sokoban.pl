:-include(game).

crossMerge([[]], List, List).

crossMerge([Move | Moves], [Push | Pushes], Result):-
	crossMerge(Moves, Pushes, ResultPom),
	append(Move, [Push | ResultPom], Result).
/*	

dfs(State, _Visited, [[]], [] ):-format('|| Boxes: ~w~n', State),
	final_state(sobokan, State).


dfs(State, Visited, [Moves | RestMoves], [Push | RestPushes] ):-
movement(State, Push, Moves),
update(State, Push, NewState),
 \+ member(NewState, Visited),
dfs(NewState,[NewState | Visited], RestMoves, RestPushes ).
*/

wall(X):-
\+ noWall(X).

noWall(X) :- top(_,X),top(X,_),right(X,_),right(_,X).


solve_dfs( State, _History, [], [[]]) :-%format('|| S: ~w~n', State),
    final_state(sokoban, State), !.


solve_dfs(State, History, [[] | Moves], [push(P,Dir)|Pushes]) :-	% prefer pushing Boxes adjacent to walls and no moves
    wall(P),
    movement(State, push(P,Dir), []),		
    update(State, push(P,Dir), NewState),
    \+ member(NewState, History),  
    solve_dfs(NewState, [NewState|History], Moves, Pushes).								

solve_dfs(State, History, [[] | Moves], [Push|Pushes]) :-	% prefer pushing then moving
    movement(State, Push, []),
    update(State, Push, NewState),
    \+ member(NewState, History),  
    solve_dfs(NewState, [NewState|History], Moves, Pushes).



solve_dfs(State, History, [Move | Moves], [push(P,Dir)|Pushes]) :-	% prefer pushing Boxes adjacent to walls
    wall(P),
    movement(State, push(P,Dir), Move),	
    update(State, push(P,Dir), NewState),
    \+ member(NewState, History),  
    solve_dfs(NewState, [NewState|History], Moves, Pushes).								


solve_dfs(State, History, [Move | Moves], [Push|Pushes]) :-
    movement(State, Push, Move),
    update(State, Push, NewState),
    \+ member(NewState, History),  
    solve_dfs(NewState, [NewState|History], Moves, Pushes).


solve_problem(Sokoban, Solution):-
	
	initial_state(Sokoban, Initial),
	solve_dfs(Initial, [Initial], Moves, Pushes),
	crossMerge(Moves, Pushes, Solution).	


solve(Problem, Solution):-
/***************************************************************************/
/* Your code goes here                                                     */
/* You can use the code below as a hint.                                   */
/***************************************************************************/
    
Problem = [Tops, Rights, Boxes, Solutions, sokoban(Sokoban)],
abolish_all_tables,
retractall(top(_,_)),
findall(_, ( member(P, Tops), assert(P) ), _),
retractall(right(_,_)),
findall(_, ( member(P, Rights), assert(P) ), _),
retractall(solution(_)),
findall(_, ( member(P, Solutions), assert(P) ), _),

retractall(initial_state(_,_)),
findall(Box, member(box(Box), Boxes), BoxLocs),
assert(initial_state(sokoban, state(Sokoban, BoxLocs))),
solve_problem(sokoban, Solution).

