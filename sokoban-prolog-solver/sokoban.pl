:-include(game).

crossMerge([[]], List, List).

crossMerge([Move | Moves], [Push | Pushes], Result):-
	crossMerge(Moves, Pushes, ResultPom),
	append(Move, [Push | ResultPom], Result).

wall(X):-
\+ noWall(X).

noWall(X) :- top(_,X),top(X,_),right(X,_),right(_,X).


dfs( State, _History, [], [[]]) :-%format('|| S: ~w~n', State),
    final_state(sokoban, State), !.


dfs(State, History, [push(P,Dir)|Pushes], [[] | Moves]) :-	% prefer pushing Boxes adjacent to walls and no moves and stop whe solution reached
    \+solution(P),
    wall(P),
    movement(State, push(P,Dir), []),		
    update(State, push(P,Dir), NewState),
    \+ member(NewState, History),  
    dfs(NewState, [NewState|History], Pushes, Moves), !.								


dfs(State, History, [push(P,Dir)|Pushes], [Move | Moves]) :-	% prefer pushing Boxes adjacent to walls
    \+solution(P),
    wall(P),
    movement(State, push(P,Dir), Move),	
    update(State, push(P,Dir), NewState),
    \+ member(NewState, History),  
    dfs(NewState, [NewState|History], Pushes, Moves), !.								

dfs(State, History, [Push|Pushes], [[] | Moves]) :-	% prefer pushing then moving (i. e. move same )
    movement(State, Push, []),
    update(State, Push, NewState),
    \+ member(NewState, History),  
    dfs(NewState, [NewState|History], Pushes, Moves), !.

dfs(State, History, [push(P,Dir)|Pushes], [Move | Moves]) :-	% when movement doesnt count chose push near solution
    solution(newP),
    neib(P, newP),
    movement(State, push(P,Dir), Move),
    update(State, push(P,Dir), NewState),
    \+ member(NewState, History),  
    dfs(NewState, [NewState|History], Pushes, Moves), !.

dfs(State, History, [Push|Pushes], [Move | Moves]) :-
    movement(State, Push, Move),
    update(State, Push, NewState),
    \+ member(NewState, History),  
    dfs(NewState, [NewState|History], Pushes, Moves), !.


solve_problem(Sokoban, Solution):-
	initial_state(Sokoban, Initial),
	dfs(Initial, [Initial], Pushes, Moves),
        crossMerge(Moves, Pushes, Solution), !.


solve(Problem, Solution):-
Problem = [Tops, Rights, Boxes, Solutions, sokoban(Sokoban)],
abolish_all_tables,
retractall(top(_,_)),
findall(_, ( member(P, Tops), assert(P) ), _),
retractall(right(_,_)),
findall(_, ( member(P, Rights), assert(P) ), _),
retractall(solution(_)),
findall(_, ( member(P, Solutions), assert(P) ), _),

retractall(initial_state(_,_)),
findall(Box, member(box(Box), Boxes), BoxLocs),
assert(initial_state(sokoban, state(Sokoban, BoxLocs))),
solve_problem(sokoban, Solution), !.

